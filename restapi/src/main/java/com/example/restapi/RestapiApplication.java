package com.example.restapi;

import org.apache.catalina.core.ApplicationContext;
import org.apache.tomcat.jni.Address;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestapiApplication {

	public static void main(String[] args) {
	 ApplicationContext context =	SpringApplication.run(RestapiApplication.class, args);
	Dress dress = context.getBean(Dress.class);
	System.out.println("instance :"+dress);
	dress.wear();
	}

}
